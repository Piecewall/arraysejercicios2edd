/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividaddos;

/**
 *
 * @author Orlando
 */
public class ActividadDos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    private static class Challenge{
	public static int[][] squarePatch(int n) {
		int[][] finalArray = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				finalArray[i][j] = n;
			}
		}
		return finalArray;
	}
    }
    
    private static class Program {
	public static int[] arrayOfMultiples(int num, int length) {
		int [] arr = new int [length];
		for (int i = 0; i < length; i++) {
			arr[i] = num * (i+1);
		}
	return arr;
	}
    }
    private static class Program3 {
    public static int getLastItem(int[] nums) {
      return nums[nums.length-1];
    }
    }
    private static class Program4 {
	public static int findLargestNum(int[] nums) {
		int i;
		int max = nums[0];
		for(i=1;i<nums.length;i++)
			if(nums[i]>max)
				max=nums[i];
		return max;
	}
    }
    
    private static class SmallestIntegerFinder {
    public static int findSmallestInt(int[] args) {
		int i;
		int min = args[0];
		for(i=1;i<args.length;i++)
			if(args[i]<min)
				min=args[i];
		return min;
    }
     }
    } 

